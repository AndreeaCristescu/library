package com.sda.library.service;


import com.sda.library.model.User;
import java.util.List;


public interface UserService {

    User createUser (User user);

    List<User> getAllUsers();

    void deleteUserById(Integer id);

    User findById(Integer id);

    Boolean findUser(Integer id);


}
