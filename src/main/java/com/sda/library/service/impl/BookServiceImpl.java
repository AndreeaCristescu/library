package com.sda.library.service.impl;

import com.sda.library.exception.BookNotFoundException;
import com.sda.library.model.Book;
import com.sda.library.repository.BookRepository;
import com.sda.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {


    private final BookRepository bookRepository;

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book createBook(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }

    @Override
    public void deleteBookById(Integer id) {
        Optional<Book> bookToBeDeleted = bookRepository.findById(id);
        if (bookToBeDeleted.isPresent()) {
            bookRepository.deleteById(id);
        } else {
            throw new BookNotFoundException("Book with id " + id + " does not exist.");
        }
    }

    @Override
    public Book findBookById(Integer id) {
        return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException("Book with id <" + id + "> does not exist."));
    }

    @Override
    public Book findByTitle(String title) {
        return bookRepository.findByTitle(title).orElseThrow(() -> new BookNotFoundException("Book with title <" + title + "> does not exist."));
    }

    @Override
    public List<Book> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy));
        Page<Book> bookPage = bookRepository.findAll(pageable);
        return bookPage.getContent();
    }

    @Override
    public void updateById(Book book) {
        Book bookToBeUpdated = findBookById(book.getId());
        bookToBeUpdated.setId(book.getId());
        bookToBeUpdated.setTitle(book.getTitle());
        bookToBeUpdated.setAuthor(book.getAuthor());
        bookToBeUpdated.setDescription(book.getDescription());
        bookToBeUpdated.setPrice(book.getPrice());
        bookToBeUpdated.setPicture(book.getPicture());

        bookRepository.save(book);
    }
}





