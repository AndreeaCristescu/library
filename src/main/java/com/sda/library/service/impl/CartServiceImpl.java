package com.sda.library.service.impl;


import com.sda.library.model.Cart;
import com.sda.library.repository.CartRepository;
import com.sda.library.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;


    @Autowired
    public CartServiceImpl(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    @Override
    public Cart save(Cart cart) {
       return cartRepository.save(cart);
    }


}
