package com.sda.library.service.impl;


import com.sda.library.exception.UserNotFoundException;
import com.sda.library.model.User;
import com.sda.library.repository.UserRepository;
import com.sda.library.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }


    @Override
    public List<User> getAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    @Override
    public void deleteUserById(Integer id) {
        Optional<User> userToBeDeleted = userRepository.findById(id);
        if(userToBeDeleted.isPresent()) {
            userRepository.deleteById(id);
        } else {
            throw new UserNotFoundException("User with id " + id + " does not exist.");
        }
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new UserNotFoundException("User not found"));
    }


    @Override
    public Boolean findUser(Integer id){
        try{
            findById(id);
        } catch (UserNotFoundException e){
            return false;
        }
        return true;
    }


}
