package com.sda.library.service;

import com.sda.library.model.Book;
import java.util.List;


public interface BookService {
    Book createBook (Book book);

    List<Book> getAllBooks();

    void deleteBookById(Integer id);

    Book findBookById(Integer id);

    Book findByTitle(String title);

    List<Book> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy);

    void updateById(Book book);

}
