package com.sda.library.service;


import com.sda.library.model.Cart;


public interface CartService {
    Cart save (Cart cart);
}
