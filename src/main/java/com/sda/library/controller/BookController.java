package com.sda.library.controller;

import com.sda.library.model.Book;
import com.sda.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/books")
@ControllerAdvice
@CrossOrigin
public class BookController {


    private final BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/create")
    public Book createBook(@RequestBody Book book) {
        return bookService.createBook(book);
    }


    @GetMapping("/all")
    public List<Book> getBooksList() {
        return bookService.getAllBooks();
    }

    @GetMapping("/findById")
    public Book findById(@RequestParam Integer id) {
        return bookService.findBookById(id);
    }

    @GetMapping("/findByTitle")
    public Book findByTitle(@RequestParam String title) {
        return bookService.findByTitle(title);
    }

    @GetMapping("/getAllPaginated")
    public List<Book> getAllPaginated(
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "50") Integer pageSize,
            @RequestParam(defaultValue = "title") String sortBy) {
        return bookService.getAllPaginated(pageNumber, pageSize, sortBy);
    }

    @DeleteMapping(path = { "/{id}" })
    public void deleteBook(@PathVariable ("id") Integer id) {
        bookService.deleteBookById(id);
    }

    @PutMapping("/update")
    public void updateById(@RequestBody Book book) {
        bookService.updateById(book);
    }

    @GetMapping
    public void getAllProductPriceGreaterThan(@RequestParam Double price){

    }

}
