package com.sda.library.controller;


import com.sda.library.model.Cart;
import com.sda.library.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
@CrossOrigin
public class CartController {

    CartService cartService;

    @Autowired
    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @PostMapping("/save")
    public Cart saveCart(@RequestBody Cart cart){
       return cartService.save(cart);
    }
}
