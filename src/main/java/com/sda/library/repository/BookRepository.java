package com.sda.library.repository;

import com.sda.library.model.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {

    Optional<Book> findByTitle(String title);
}
