package com.sda.library.repository;

import com.sda.library.model.Book;
import com.sda.library.model.Cart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Integer> {
}
